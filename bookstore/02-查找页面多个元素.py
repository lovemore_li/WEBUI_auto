from selenium import webdriver
from time import sleep
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager

driver = webdriver.Chrome()
driver.maximize_window()
url = 'http://novel.hctestedu.com/'
driver.implicitly_wait(20)
driver.get(url=url)
try:
    link_love = driver.find_element(By.XPATH, '//dl[@id="topBooks1"]//a')
    link_love.click()
    assert driver.title != '读书屋_原创小说网站'
    print("测试通过")
    driver.back()
    book_lst = driver.find_elements(By.XPATH, '//*[@id="topBooks1"]/dd/a')
    print("书数量{}".format(len(book_lst)))
    for i in range(len(book_lst)):
        book_lst = driver.find_elements(By.XPATH, '//*[@id="topBooks1"]/dd/a')
        book_lst[i].click()
        assert driver.title != '读书屋_原创小说网站'
        print("测试通过")
        driver.back()

except Exception as e:
    print("信息抛出异常，错误是：{}".format(e))
finally:
    driver.quit()
