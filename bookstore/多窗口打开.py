from selenium import webdriver
from time import sleep
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager

try:
    driver = webdriver.Chrome()
    driver.maximize_window()
    url = 'https://sz.ganji.com/'
    driver.implicitly_wait(20)
    driver.get(url=url)
    bcbz = driver.find_element(By.XPATH, '//div[@class="top-head-col"][1]')
    bcbz.click()
    print(driver.title)
    handle = driver.current_window_handle
    print(handle)
    handles = driver.window_handles
    print("当前的handles是{}".format(handles))
    driver.switch_to.window(handles[1])
    print("切换句柄后的handle的title{}".format(driver.title))
    driver.close()
    # print(driver.title)
    driver.switch_to.window(handle)
    print(driver.title)
except Exception as e:
    print("错误是{}".format(e))
finally:
    driver.quit()
