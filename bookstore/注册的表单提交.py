from PIL import Image
from selenium.webdriver.common.by import By
from selenium import webdriver
import requests
import pytesseract
from time import sleep


# 获取图片
def get_image(img_path):
    img = Image.open(img_path)
    return img


def image_grayscale_deal(image):
    img = image.convert("L")
    # img.show()  # 展示灰度处理之后的图片
    return img


# 图片二值化处理
def image_threashoding_method(image):
    threshold = 160
    table = []
    for i in range(256):
        if i < threshold:
            table.append(0)
        else:
            table.append(1)

    image = image.point(table, "1")
    # image.show()
    return image


# 图像识别
def captcha_crack(image):
    result = pytesseract.image_to_string(image)
    return result


# 清空input输入框
def clear_all_input():
    lst = driver.find_elements(By.TAG_NAME, "input")
    for i in range(1, 4):
        lst[i].clear()


try:
    driver = webdriver.Chrome()
    driver.maximize_window()
    url = 'http://novel.hctestedu.com/'
    driver.implicitly_wait(10)
    driver.get(url=url)
    register = driver.find_element(By.XPATH, '//span[@class="user_link"]/a[2]')
    register.click()
    sleep(5)

    # 获取图片
    verification_img = driver.find_element(By.XPATH, '//img[@class="code_pic"]')
    verification_img.screenshot('./image/1.png')
    sleep(5)

    # 对图片进行处理操作  -- 灰度处理
    img1 = get_image("./image/1.png")
    img2 = image_grayscale_deal(img1)
    # 对图片进行  二值化处理
    img3 = image_threashoding_method(img2)
    # 最终结果
    result = captcha_crack(img3)
    print("根据图像识别，获取的最终结果是:{}".format(result))
    num = result.split("\n")
    print(num[0])

    mobile_input = driver.find_element(By.XPATH, '//input[@name="txtUName"]')
    psword_input = driver.find_element(By.XPATH, '//input[@name="txtPassword"]')
    verifi_input = driver.find_element(By.XPATH, '//input[@name="TxtChkCode"]')
    register_button = driver.find_element(By.XPATH, '//input[@name="btnRegister"]')
    check_text = driver.find_element(By.XPATH, '//span[@id="LabErr"]')

    mobile_input.send_keys("122")
    psword_input.send_keys("121")
    verifi_input.send_keys(num[0])
    register_button.click()
    assert '手机号格式不正确！' in check_text.text
    clear_all_input()
    print("测试通过")

    mobile_input.send_keys("")
    psword_input.send_keys("")
    verifi_input.send_keys(num[0])
    register_button.click()
    assert '手机号不能为空！' in check_text.text
    clear_all_input()
    print("测试通过")

    mobile_input.send_keys("13421281484")
    psword_input.send_keys("123456")
    verifi_input.send_keys("1234")
    register_button.click()
    assert '验证码' in check_text.text
    clear_all_input()
    print("测试通过")
except AssertionError:
    print("'验证码错误！'不等于{}".format(check_text.text))
