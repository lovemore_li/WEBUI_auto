from selenium import webdriver
from time import sleep
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager

try:
    driver = webdriver.Chrome()
    driver.maximize_window()
    url = 'https://sz.ganji.com/'
    driver.implicitly_wait(20)
    driver.get(url=url)
    picture_file = driver.save_screenshot("./picture/test1.png")
    print("%s:截图成功！！！"% picture_file)
except BaseException as msg:
    print(msg)