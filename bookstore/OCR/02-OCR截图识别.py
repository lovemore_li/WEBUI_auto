from PIL import Image
from selenium.webdriver.common.by import By
from selenium import webdriver
import requests
import pytesseract
from time import sleep


# 获取图片
def get_image(img_path):
    img = Image.open(img_path)
    return img


def image_grayscale_deal(image):
    img = image.convert("L")
    img.show()  # 展示灰度处理之后的图片
    return img


# 图片二值化处理
def image_threashoding_method(image):
    threshold = 160
    table = []
    for i in range(256):
        if i < threshold:
            table.append(0)
        else:
            table.append(1)

    image = image.point(table, "1")
    image.show()
    return image


# 图像识别
def captcha_crack(image):
    result = pytesseract.image_to_string(image)
    return result


try:
    driver = webdriver.Chrome()
    driver.maximize_window()
    url = 'http://novel.hctestedu.com/'
    driver.implicitly_wait(10)
    driver.get(url=url)
    register = driver.find_element(By.XPATH, '//span[@class="user_link"]/a[2]')
    register.click()
    sleep(10)
    # 获取图片
    verification_img = driver.find_element(By.XPATH, '//img[@class="code_pic"]')
    verification_img.screenshot('./image/2.png')
    sleep(10)

    # 对图片进行处理操作  -- 灰度处理
    img1 = get_image("../image/1.png")
    img2 = image_grayscale_deal(img1)
    # 对图片进行  二值化处理
    img3 = image_threashoding_method(img2)
    # 最终结果
    result = captcha_crack(img3)
    print("根据图像识别，获取的最终结果是:{}".format(result))
    num = result.split("\n")
    print(num[0])
except Exception():
    print()

finally:
    driver.quit()
