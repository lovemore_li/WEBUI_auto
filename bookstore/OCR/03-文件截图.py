from selenium import webdriver
from time import sleep
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
import base64

try:
    driver = webdriver.Chrome()
    driver.maximize_window()
    url = 'http://novel.hctestedu.com/'
    driver.implicitly_wait(20)
    driver.get(url=url)
    picture_file = driver.get_screenshot_as_base64()
    print(picture_file)
    img_data = base64.b64decode(picture_file)
    file = open("../picture/base.png", "wb")
    file.write(img_data)
    file.close()
except BaseException as msg:
    print(msg)
