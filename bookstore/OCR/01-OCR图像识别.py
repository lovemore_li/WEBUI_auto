from PIL import Image
from selenium.webdriver.common.by import By
from selenium import webdriver
import requests
import pytesseract


# 获取图片
def get_image(img_path):
    img = Image.open(img_path)
    return img


def image_grayscale_deal(image):
    img = image.convert("L")  # image.convert：图像处理函数，用于将图像从一种颜色模式转换为另一种颜色模式
    img.show()  # 展示灰度处理之后的图片,img.show()是PIL库中的一个方法，用于显示图像，自动调起图像查看器，通常用于调试和查看图像处理的结果。
    return img


# 图片二值化处理
def image_threashoding_method(image):
    threshold = 160  # threshold(阈值)图像处理中常用的一种操作，用于将图像的像素值转换为二值
    table = []
    for i in range(256):
        if i < threshold:
            table.append(0)
        else:
            table.append(1)

    image = image.point(table, "1")
    image.show()
    return image


# 图像识别
def captcha_crack(image):
    result = pytesseract.image_to_string(image)
    return result


try:
    driver = webdriver.Chrome()
    driver.maximize_window()
    url = 'http://novel.hctestedu.com/'
    driver.implicitly_wait(10)
    driver.get(url=url)
    register = driver.find_element(By.XPATH, '//span[@class="user_link"]/a[2]')
    register.click()
    # 获取图片
    verification_img = driver.find_element(By.XPATH, '//img[@class="code_pic"]')
    img_url = verification_img.get_attribute("src")
    print(img_url)
    img_data = requests.get(img_url, stream=True)
    print(img_data.content)
    with open("../image/1.png", "wb") as f:
        f.write(img_data.content)
    # 对图片进行处理操作  -- 灰度处理
    img1 = get_image("../image/1.png")
    img2 = image_grayscale_deal(img1)
    # 对图片进行  二值化处理
    img3 = image_threashoding_method(img2)
    # 最终结果
    result = captcha_crack(img3)
    print("根据图像识别，获取的最终结果是:{}".format(result))
    num = result.split("\n")
    print(num[0])
except Exception():
    print()

finally:
    driver.quit()
