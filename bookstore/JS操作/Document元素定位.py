from selenium import webdriver
from selenium.webdriver.common.by import By
from time import sleep

url = 'http://novel.hctestedu.com/'
driver = webdriver.Chrome()
driver.implicitly_wait(10)
driver.maximize_window()
driver.get(url=url)

# js 元素定位
# js_id = "document.getElementById('searchKey').value='365'"
# driver.execute_script(js_id)


# class元素定位
js_class = "document.getElementsByClassName('s_int')[0].value='无尽沉沦'"
driver.execute_script(js_class)
sleep(2)

js_click = "document.getElementById('btnSearch').click()"
driver.execute_script(js_click)
sleep(2)
