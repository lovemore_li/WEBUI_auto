"""
通过cookie机制 跳过验证码操作
"""
from selenium import webdriver
from time import sleep
from selenium.webdriver.common.by import By

url = 'http://novel.hctestedu.com/'
driver = webdriver.Chrome()
driver.implicitly_wait(10)
driver.maximize_window()
driver.get(url=url)
login_button = driver.find_element(By.XPATH, '//a[@class="mr15"]')
login_button.click()
mobile_input = driver.find_element(By.XPATH, '//input[@placeholder="手机号码"]')
passwd_input = driver.find_element(By.XPATH, '//input[@name="txtPassword"]')
login_bt = driver.find_element(By.XPATH, '//input[@name="btnLogin"]')
mobile_input.send_keys("13421281484")
passwd_input.send_keys("123456")
login_bt.click()
assert "会员登录_读书屋" in driver.title

before_cookies = driver.get_cookies()
print(before_cookies)
driver.refresh()
after_cookies = driver.get_cookies()
print(after_cookies)
driver.quit()


driver = webdriver.Chrome()
driver.get(url=url)
driver.maximize_window()
driver.delete_all_cookies()
for cookie in after_cookies:
    # selenium 的cookies 不支持expiry
    if "expiry" in cookie.keys():
        cookie.pop("expiry")
    driver.add_cookie(cookie)
driver.refresh()
sleep(20)
print("111")
