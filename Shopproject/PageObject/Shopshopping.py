"""
1.输入商品名称
2.点击搜索
3.点击商品货物
4.跳转商品货物新窗口
5.选择套餐、配色
6.加入购物车
"""
from Shopproject.Base.BasePage import BasePage


class Shopshop(BasePage):
    def open_url(self):
        url = "http://116.63.181.151/"
        self.get_url(url=url)

    def text_send(self, loc, text):
        self.input_text(loc, text)

    def click(self, loc):
        self.click_option(loc)

    def new_handle(self):
        self.switch_handle(1)

    def loca_text(self, loc):
        self.locat_text(loc)

    def iframe_switch(self, loc):
        self.switch_iframe(loc)

    def iframe_default(self):
        self.switch_to_default_content()
