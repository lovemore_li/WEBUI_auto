"""
1.打开商城
2.输入商品名称
3.点击搜索
"""
from Shopproject.Base.BasePage import BasePage


class ShopSearch(BasePage):
    def open_shop(self):
        url = "http://116.63.181.151/"
        self.get_url(url=url)

    def text_send(self, loc, text):
        self.input_text(loc, text)

    def click(self, loc):
        self.click_option(loc)


