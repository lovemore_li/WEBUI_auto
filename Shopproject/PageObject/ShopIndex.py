"""
1.打开商城
2.点击登录
3.输入账号
"""
from Shopproject.Base.BasePage import BasePage


class ShopIndex(BasePage):
    def open_shop(self):
        url = "http://116.63.181.151/"
        self.get_url(url=url)

    def text_send(self, loc, text):
        self.input_text(loc, text)

    def click(self, loc):
        self.click_option(loc)
