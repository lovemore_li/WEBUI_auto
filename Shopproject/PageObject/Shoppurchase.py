"""
1.打开网页
2.搜索内容
3.点击对应内容
4.选择款式
5.登录账号
6.重新选择款式
7.点击购买
8.新建收货地址
"""

from Shopproject.Base.BasePage import BasePage


class Shopbuy(BasePage):
    def open_url(self):
        url = "http://116.63.181.151/"
        self.get_url(url=url)

    def text_send(self, loc, text):
        self.input_text(loc, text)

    def click_button(self, loc):
        self.click_option(loc)

    def new_handle(self, num):
        self.switch_handle(num)

    def loca_text(self, loc):
        self.locat_text(loc)

    def iframe_switch(self, loc):
        self.switch_iframe(loc)

    def iframe_default(self):
        self.switch_to_default_content()

    def enter_simulate(self, loc):
        self.enter_simulate(loc)

    def quit_browser(self):
        self.driver.quit()
