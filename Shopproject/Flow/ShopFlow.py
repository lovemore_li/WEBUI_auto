from Shopproject.PageObject.ShopIndex import ShopIndex
from Shopproject.PageObject.ShopSearch import ShopSearch
from Shopproject.PageObject.Shopshopping import Shopshop
from Shopproject.PageObject.Shoppurchase import Shopbuy
from time import sleep


class ShopFlow00(ShopIndex):
    def sh_login(self, keyword1, keyword2):
        self.open_shop()
        self.click(('xpath', '//div[@class="menu-hd"]/a[1]'))
        self.text_send(('xpath', '(//input[@name="accounts"])[1]'), keyword1)
        self.text_send(('xpath', '(//input[@name="pwd"])[1]'), keyword2)
        self.click(('xpath', '(//button[@type="submit"])[3]'))
        sleep(2)
        result = self.locat_text(('xpath', '//div[@class="menu-hd"]/em[2]'))
        return result


class ShopFlow01(ShopSearch):
    def sh_search(self, text):
        self.open_shop()
        self.text_send(('xpath', '//input[@id="search-input"]'), text)
        self.click(('xpath', '//button[@id="ai-topsearch"]'))
        result = self.page_title()
        return result


class ShopFlow02(Shopshop):
    def sh_shoping(self, text):
        self.open_url()
        self.text_send(('xpath', '//input[@id="search-input"]'), text)
        sleep(1)
        self.click(('xpath', '//button[@id="ai-topsearch"]'))
        self.click(('xpath', '//img[@class="goods-images"]'))
        self.new_handle()
        self.click(('xpath', '(//li[@class="sku-line "])[1]'))
        self.click(('xpath', '//li[@data-value="银色"]'))
        self.click(('xpath', '//li[@data-value="128G"]'))
        self.click(('xpath', '//button[@title="加入购物车"]'))

        self.iframe_switch(('xpath', '//div[@class="am-popup-inner"]/iframe'))

        self.text_send(('xpath', '//input[@placeholder="请输入用户名/手机/邮箱"]'), 'chenqixiao')
        self.text_send(('xpath', '(//input[@name="pwd"])'), 'chenqixiao')
        self.click(('xpath', '(//form[@novalidate="novalidate"]/div/button)[1]'))

        self.iframe_default()

        self.click(('xpath', '(//li[@class="sku-line "])[1]'))
        self.click(('xpath', '//li[@data-value="银色"]'))
        self.click(('xpath', '//li[@data-value="128G"]'))
        self.click(('xpath', '//button[@title="加入购物车"]'))
        self.click(('xpath', '(//div[@class="menu-hd "]/a/span)[2]'))
        result = self.page_title()
        return result


class ShopFlow03(Shopbuy):
    def sh_shopbuying(self, text):
        self.open_url()
        self.text_send(('xpath', '//input[@id="search-input"]'), text)
        self.click_button(('xpath', '//button[@id="ai-topsearch"]'))
        self.click_button(('xpath', '//div[@class="items am-padding-bottom-xs"]'))
        self.new_handle(1)
        self.click_button(('xpath', '//li[@data-value="白色"]'))
        self.click_button(('xpath', '//li[@data-value="M+M"]'))
        self.click_button(('xpath', '//button[@data-type="buy"]'))

        self.iframe_switch(('xpath', '//div[@class="am-popup-inner"]/iframe'))

        self.text_send(('xpath', '//input[@placeholder="请输入用户名/手机/邮箱"]'), 'chenqixiao')
        self.text_send(('xpath', '(//input[@name="pwd"])'), 'chenqixiao')
        self.click_button(('xpath', '(//form[@novalidate="novalidate"]/div/button)[1]'))

        self.click_button(('xpath', '//li[@data-value="白色"]'))
        sleep(1)
        self.click_button(('xpath', '//li[@data-value="M+M"]'))
        self.click_button(('xpath', '//button[@data-type="buy"]'))

        self.click_button(('xpath', '//div[@class="table-no"]/button'))


"""  # 报错部分，后面再处理
        self.iframe_switch(('xpath', '//div[@id="am-modal-ehy8w"]//iframe'))
        self.text_send(('xpath', '//input[@placeholder="别名"]'), "马里奥")
        self.text_send(('xpath', '//input[@placeholder="姓名"]'), "马里奥")
        self.text_send(('xpath', '//input[@placeholder="电话"]'), "13800000000")
        self.text_send(('xpath', '(//a[@class="chosen-single chosen-default"])[1]'), "北京")
        self.enter_simulate('xpath', '(//a[@class="chosen-single chosen-default"])[1]')
        self.text_send(('xpath', '(//a[@class="chosen-single chosen-default"])[2]'), "东城")
        self.enter_simulate('xpath', '(//a[@class="chosen-single chosen-default"])[2]')
        self.text_send(('xpath', '(//a[@class="chosen-single chosen-default"])[3]'), "东四")
        self.enter_simulate('xpath', '(//a[@class="chosen-single chosen-default"])[3]')
        self.text_send(('xpath', '//input[@name="address"]'), "北京市东城区东四街道")
        self.click_button('xpath', '//div[@class="am-form-group am-form-group-refreshing"]/button')

        self.iframe_default()

"""

