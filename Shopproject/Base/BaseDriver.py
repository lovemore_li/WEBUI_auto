from selenium import webdriver


class BrowserDriver(object):
    def open_browser(self):
        driver = webdriver.Chrome()
        driver.implicitly_wait(15)
        return driver
