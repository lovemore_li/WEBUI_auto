from selenium.webdriver import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


class BasePage(object):
    def __init__(self, driver):
        self.driver = driver
        self.driver.maximize_window()

    """打开页面"""

    def get_url(self, url):
        self.driver.get(url)

    """关闭浏览器"""

    def quit_browser(self):
        self.driver.quit()

    """元素定位"""

    def location(self, loc):
        mem = self.driver.find_element(*loc)
        return mem

    """字符输入"""

    def input_text(self, loc, text):
        mem = self.location(loc)
        mem.clear()
        mem.send_keys(text)

    """清空输入框"""

    def clear(self, loc):
        mem = self.location(loc)
        mem.clear()

    """点击"""

    def click_option(self, loc):
        mem = self.location(loc)
        mem.click()

    """元素定位文本信息"""

    def locat_text(self, loc):
        mem = self.location(loc)
        return mem.text

    """页面title"""

    def page_title(self):
        return self.driver.title

    """页面URL"""

    def page_url(self):
        return self.driver.current_url

    """切换新的窗口"""

    def switch_handle(self, num):
        handles = self.driver.window_handles
        for handle in handles:
            if handle != self.driver.current_window_handle:
                self.driver.switch_to.window(handles[num])

    """切换iframe"""

    def switch_iframe(self, loc):
        WebDriverWait(self.driver, 10).until(EC.frame_to_be_available_and_switch_to_it(loc))

    """iframe切换主页面"""

    def switch_to_default_content(self):
        self.driver.switch_to.default_content()

    """模拟enter回车操作"""

    def enter_simulate(self, loc):
        mem = self.location(loc)
        mem.send_keys(Keys.ENTER)
