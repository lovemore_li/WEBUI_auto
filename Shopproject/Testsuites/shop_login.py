from time import sleep
import pytest
from Shopproject.Base.BaseDriver import BrowserDriver
from Shopproject.Flow.ShopFlow import ShopFlow00, ShopFlow03
from Shopproject.Flow.ShopFlow import ShopFlow01
from Shopproject.Flow.ShopFlow import ShopFlow02


class TestShop():
    # def test_hop_login(self):
    #     browser = BrowserDriver()
    #     self.driver = browser.open_browser()
    #     shopproject = ShopFlow00(self.driver)
    #     result = shopproject.sh_login("chenqixiao", "chenqixiao")
    #     assert "chenqixiao" in result
    #     print("测试通过")
    #     self.driver.quit()
    #
    # def test_shop_search(self):
    #     browser = BrowserDriver()
    #     self.driver = browser.open_browser()
    #     shopproject = ShopFlow01(self.driver)
    #     result = shopproject.sh_search("苹果")
    #     assert "苹果" in result
    #     print("测试通过")
    #     self.driver.quit()
    #
    # def test_shop_shoping(self):
    #     browser = BrowserDriver()
    #     self.driver = browser.open_browser()
    #     shopproject = ShopFlow02(self.driver)
    #     result = shopproject.sh_shoping("苹果")
    #     assert "用户中心" in result
    #     print("测试通过")
    #     self.driver.quit()

    def test_shop_buy(self):
        browser = BrowserDriver()
        self.driver = browser.open_browser()
        shopproject = ShopFlow03(self.driver)
        result = shopproject.sh_shopbuying("裙子")
        assert "订单确认" in result
        print("测试通过")
        self.driver.quit()


if __name__ == '__main__':
    pytest.main(['-k', 'test_shop_buy'])
