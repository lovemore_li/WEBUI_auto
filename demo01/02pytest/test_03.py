"""
fixture:全局生效
fixture(arg)
arg：scope  有四个级别 function(默认)、class、module、session
arg：params：可选参数列表，可以多个参数调用fixture功能，同时支持所有测试使用
arg：autos：
arg：ids  每个字符串id列表
arg：name fixture的名字
返回值用的不是return，而是yield
"""

import pytest


def test_1(login):
    print("test1,登录后执行")


def test_2():
    print("test2,不用登录也可执行")


def test_3(login):
    print("test3,需要登录")


if __name__ == '__main__':
    pytest.main(['-vs'])
