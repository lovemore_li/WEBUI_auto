"""
pytest里也包含 setUp 和 teardown对应的内容
用例运行级别
模块级（setup_module/teardown_module）：开始与模块的始末，全局的
函数级（setup_function/teardown_module）:只对函数用例生效（不在类中:当在类中时，不执行）
类级（setup_class/teardown_class）：只在类中前后运行一次
方法级（setup_method/teardown_method）：开始于方法始末
类里面的（setup/teardown）：运行在调用方法的前后

"""
import pytest


def setup_function():
    print("每个用例前都会对我执行一次：setup")


def teardown_function():
    print("每个用例后都会对我执行一次：teardown")


def test_one():
    str = "1234"
    assert "2" in str


def test_two():
    a = 2 + 2
    assert a == 5

if __name__ == '__main__':
    pytest.main(['-vs'])