import unittest
from unittest import skip

"""
跳过测试  skip装饰器
"""


class test04(unittest.TestCase):
    def setUp(self) -> None:
        print("setup")

    def tearDown(self) -> None:
        print("teardown")

    def test_a(self):
        print("a")

    @unittest.skip
    def test_b(self):
        print("b")


if __name__ == '__main__':
    unittest.main()
