import unittest
from selenium import webdriver
from time import sleep
from selenium.webdriver.common.by import By
from ddt import ddt, data, unpack


@ddt
class Test02(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()
        self.driver.get("http://novel.hctestedu.com/")

    def tearDown(self) -> None:
        self.driver.quit()

    @data("无尽沉沦", "365")
    def test_1(self, txt):
        seach_input = self.driver.find_element(By.ID, 'searchKey')
        seach_input.send_keys(txt)
        seach_button = self.driver.find_element(By.ID, 'btnSearch')
        seach_button.click()


if __name__ == '__main__':
    unittest.main()
