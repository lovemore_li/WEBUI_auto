"""
无尽,最强
宠,反派
"""

import unittest
from ddt import ddt, data, unpack
from selenium import webdriver
from selenium.webdriver.common.by import By

"""

@ddt
class Test02(unittest.TestCase):
    def setUp(self) -> None:
        pass

    def tearDown(self) -> None:
        pass

    @data(("无尽沉沦", "365"), ("总", "少爷"))
    @unpack  # 装饰器 用来解包data内的值
    def test_1(self, txt, txt1):
        print(txt)
        print(txt1)


if __name__ == '__main__':
    unittest.main()
    
"""


def readfile():
    lst = []
    file = open('file.txt', 'r', encoding='utf-8')
    for line in file.readlines():
        lst.append(line.strip('\n').split(','))
    return lst


@ddt
class Test02(unittest.TestCase):
    def setUp(self) -> None:
        pass

    def tearDown(self) -> None:
        pass

    @data(*readfile())
    @unpack  # 装饰器 用来解包data内的值
    def test_1(self, txt, txt1):
        print(txt)
        print(txt1)


if __name__ == '__main__':
    unittest.main()
