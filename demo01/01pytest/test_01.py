import pytest


class TestClass():
    @pytest.mark.slow
    def test_one(self):
        print("这是我想要打印的内容")
        str = "turbo"
        assert "t" in str

    def test_two(self):
        str = "mike"
        assert "L" in str

    def test_add(self):
        assert 4 == 4


if __name__ == '__main__':
    pytest.main(['-vs', 'test_01.py'])
