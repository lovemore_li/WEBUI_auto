"""
流程类--将测试步骤打包到一起
流程：
肆意组合页面中的内容 操作
"""

from day06pom.pageObjects.Dushuwu_index import DushuwuIndex
from time import sleep
from selenium.webdriver.common.by import By


class dushuwuFlow(DushuwuIndex):
    def dushuwu_seach(self, selector, keyword):
        self.open_dushuwu()
        self.type_send(selector, keyword)  # 输入搜索关键字
        sleep(1)  # 加入强制等待
        self.click((By.ID, "btnSearch"))
        sleep(1)
        result = self.get_page_title()
        return result

    def dushuwu_login(self, selector1, selector2, keyword1, keyword2):
        self.open_dushuwu()
        self.click((By.XPATH, '//a[@class="mr15"]'))
        sleep(1)
        self.type_send(selector1, keyword1)
        self.type_send(selector2, keyword2)
        self.click((By.XPATH, '//input[@name="btnLogin"]'))
        sleep(1)
        result = self.location_text((By.XPATH, '//a[@class="mr15"]'))
        return result

    def dushuwu_allworks(self):
        self.open_dushuwu()
        self.click((By.LINK_TEXT, '全部作品'))
        sleep(1)
        result = self.get_page_title()
        return result
