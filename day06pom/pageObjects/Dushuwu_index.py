"""
1.打开读书屋
2.搜索关键词
3.获取title
页面类：
封装在当前页面或者这个流程上的基本方法便于后续的复用以及维护
"""

from day06pom.Base.BasePage import BasePage


class DushuwuIndex(BasePage):
    # 调用父类的方法
    def open_dushuwu(self):
        self.get_url("http://novel.hctestedu.com/")

    def type_send(self, selector, text):
        self.text_send(selector, text)

    def click(self, selector):
        self.click_option(selector)
