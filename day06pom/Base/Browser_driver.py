import os.path
from selenium import webdriver


class BrowserDriver(object):
    def open_browser(self, browser):
        """设定浏览器类型"""
        if "Chrome" == browser:
            chrome_driver_path = os.path.dirname(os.path.abspath("."))
            driver = webdriver.Chrome()
        else:
            fire_driver_path = os.path.dirname(os.path.abspath("."))+"/"

        driver.implicitly_wait(15)
        return driver
