from selenium import webdriver


class BasePage(object):
    def __init__(self, driver):
        self.driver = driver
        self.driver.maximize_window()


    def get_url(self, url):
        self.driver.get(url)

    # 关闭浏览器
    def quit_browser(self):
        self.driver.quit()

    # 获取元素 arr = []
    def findElement(self, selector):
        ele = self.driver.find_element(*selector)
        return ele

    # 输入字符
    def text_send(self, selector, text):
        ele = self.findElement(selector)
        ele.clear()
        ele.send_keys(text)

    # 清空
    def clear(self, selector):
        ele = self.findElement(selector)
        ele.clear()

    # 点击
    def click_option(self, selector):
        ele = self.findElement(selector)
        ele.click()

    # 获取元素定位文本
    def location_text(self, selector):
        ele = self.findElement(selector)
        return ele.text

    # 获取页面title
    def get_page_title(self):
        return self.driver.title

    # 获取页面url断言
    def get_page_url(self):
        return self.driver.current_url

    # 切换到新的窗口
    def switch_handle(self, num):
        handles = self.driver.window_handles
        for newhandle in handles:
            if newhandle != self.driver.current_window_handle:
                self.driver.switch_to.window(handles[num])
