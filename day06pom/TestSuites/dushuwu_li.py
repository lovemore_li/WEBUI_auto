from day06pom.Base.Browser_driver import BrowserDriver
from day06pom.Base.BasePage import BasePage
from day06pom.pageObjects.Dushuwu_index import DushuwuIndex
from day06pom.Flow.DushuwuFlow import dushuwuFlow
from selenium.webdriver.common.by import By

"""
封装的根本目的：将所有重复的代码抽离
"""


class TestDushuwuSearch():
    def dushuwu_1(self):
        browser = BrowserDriver()
        self.driver = browser.open_browser("Chrome")
        dushuwu = dushuwuFlow(self.driver)
        result = dushuwu.dushuwu_seach((By.ID, "searchKey"), '无尽沉沦')
        assert result == "全部作品_读书屋"
        print("测试通过")
        self.driver.quit()

    def dushuwu_2(self):
        browser = BrowserDriver()
        self.driver = browser.open_browser("Chrome")
        dushuwu = dushuwuFlow(self.driver)
        result = dushuwu.dushuwu_login((By.XPATH, '//input[@name="txtUName"]'),
                                       (By.XPATH, '//input[@name="txtPassword"]'), '13421281484', '123456')
        assert result == "13421281484"
        print("测试pass")
        self.driver.quit()

    def dushuwu_3(self):
        browser = BrowserDriver()
        self.driver = browser.open_browser("Chrome")
        dushuwu = dushuwuFlow(self.driver)
        result = dushuwu.dushuwu_allworks()
        assert result == "全部作品_读书屋"
        print("测试通过")
        self.driver.quit()


ts = TestDushuwuSearch()
ts.dushuwu_1()
ts.dushuwu_2()
ts.dushuwu_3()
